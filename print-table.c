#include "print-table.h"
#include <string.h>
#include <stdio.h>

#define MAX(a,b) \
	({ __typeof__ (a) _a = (a); \
	 __typeof__ (b) _b = (b); \
	 _a > b? _a : _b; })
#define $each(_item, _arr, _n) \
	for (__typeof__ (_arr[0]) *_item = _arr; _item < _arr + _n; _item++)

void pt_print_table (struct pt_record *records, size_t n_records, size_t n_fields) {

	// Get the longest fields
	int longest_fields[n_fields];
	memset (longest_fields, 0, n_fields * sizeof *longest_fields);
	$each (record, records, n_records) {
		if (record->unformatted) continue;
		$each (field, record->fields, n_fields) {
			if (!field)
				continue;
			longest_fields[field - record->fields] =
				MAX (*field? strlen (*field): 0,
					longest_fields[field - record->fields]);
		}
	}

	// Print it
	$each (record, records, n_records) {
		if (!record) continue;
		if (record->unformatted) {
			printf ("%s\n", record->fields[0]);
			continue;
		}
		for (char **field_p = record->fields;
				field_p - record->fields < n_fields; field_p++) {

			int len;

			if (field_p && *field_p) {
				len = strlen (*field_p);
				fputs (*field_p, stdout);
			}

			if (field_p != records->fields + n_fields - 1) {
				int next_field_index = len;
				while (next_field_index < longest_fields[field_p - record->fields] + 1) {
					putchar (' ');
					next_field_index++;
				}
			}
		}
		putchar ('\n');
	}
}


