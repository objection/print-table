#include <stddef.h>
#include <stdbool.h>

struct pt_record {

	// A record with this set won't contribute to the field width.
	// It might produce a table like this:
    //      eggs  elepant ham
    //      hello abc     hi
    //      This is some arbitrary text. Doesn't muck up the field
    // width. Is just one line
    //      More  stuff   here
	//
	//  Another note about unformatted. You're only allowed one field
	//  in that case. I can't imagine why you'd want more than one
	//  unformatted field, anyway, perhaps due to an embarrassing lack
	//  of imagination.
	bool unformatted;
	char **fields;
};

void pt_print_table (struct pt_record *records, size_t n_records, size_t n_fields);
