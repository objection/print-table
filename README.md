# Print Table

One function. Give is an array of struct record, which just has an
array of strings inside it, the number of field and it'll print it out
properly, like:
	This     That     Another_thing You_get_it
	10034234 Elephant 1             hi

It expects each record to have the same number of fields. For a
missing field, set it to NULL.
